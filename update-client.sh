#update-client.sh
#!/bin/bash

#stop client
pm2 stop all

#stop chia
cd ~/chia-blockchain/
. ./activate
chia stop all -d
chia stop all
chia stop all -d
chia stop all
deactivate

#stop chives
cd ~/chives-blockchain/
. ./activate
chives stop all -d
chives stop all
chives stop all -d
chives stop all
deactivate

#update client
cd ~/ecopool-client/
git reset --hard
git pull
yarn install

#restart client
pm2 restart all
echo "wait 10 sec."
sleep 10

####pm2 log 0
#version_client=$(curl -s http://localhost:3401/api/gui/get-version | jq ".answer.version")
#echo "client version ${version_client:1:-1}"
cd
echo "update completed!"
