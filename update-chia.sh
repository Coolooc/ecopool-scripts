#update-chia.sh
#!/bin/bash

#stop client
pm2 stop all

#stop chia
cd ~/chia-blockchain/
. ./activate
chia stop all -d
chia stop all
chia stop all -d
chia stop all
deactivate

#reinstall chia
cd
rm -rf chia-blockchain/
git clone https://github.com/Chia-Network/chia-blockchain.git -b latest --recurse-submodules
cd chia-blockchain/
sh install.sh

#restart client
pm2 restart all
echo "wait 10 sec."
sleep 10
cd ~/chia-blockchain/
. ./activate
chia init
chia init --fix-ssl-permissions
echo "chia version $(chia version)"
deactivate

####pm2 log 0
cd
echo "update completed!"
