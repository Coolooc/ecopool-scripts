#install-chives-and-client.sh
#!/bin/bash

#packages
sudo apt update
sudo apt upgrade -y
sudo apt install git curl -y
curl -sL https://deb.nodesource.com/setup_12.x | sudo bash -
sudo apt install nodejs -y

#install chives
cd
rm -rf chives-blockchain/
git clone https://github.com/HiveProject2021/chives-blockchain.git
cd chives-blockchain/
sh install.sh
sudo apt autoremove -y

#backup local.json
[ -f ~/ecopool-client/config/local.json ] && cp ~/ecopool-client/config/local.json ~/local.json.bak

#install ecopool-client part 1
cd
rm -rf ecopool-client/
git clone https://gitlab.com/ecopool-public/chia-telemetery-client.git ~/ecopool-client

[ -f local.json.bak ] && mv local.json.bak ecopool-client/config/local.json

#init chives
cd ~/chives-blockchain/
. ./activate
chives init
#chives init --fix-ssl-permissions
chives keys generate

chives start harvester -r
sleep 10
chives stop all -d
chives stop all
chives stop all -d
chives stop all
echo "chives version $(chives version)"
deactivate

#install ecopool-client part 2
cd ~/ecopool-client/

sudo npm i -g pm2
pm2 stop all
pm2 delete all

sudo npm i -g yarn
yarn install

NODE_ENV=production pm2 start npm --name ecopool-gui -- run start
pm2 save

#set startup (pm2 startup)
sudo env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u $USER --hp /home/$USER

pm2 update
pm2 restart all
echo "wait 10 sec."
sleep 10

echo ""
echo "==============================================================================="
echo "Ecopool client available on url http://localhost:3401 open it in your browser  "
echo "Клиент ecopool доступен по адресу http://localhost:3401 откройте его в браузере"
echo "==============================================================================="
