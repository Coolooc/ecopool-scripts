#update-chives.sh
#!/bin/bash

#stop client
pm2 stop all

#stop chives
cd ~/chives-blockchain/
. ./activate
chives stop all -d
chives stop all
chives stop all -d
chives stop all
deactivate

#reinstall chives
cd
rm -rf chives-blockchain/
git clone https://github.com/HiveProject2021/chives-blockchain.git
cd chives-blockchain/
sh install.sh

#restart client
pm2 restart all
echo "wait 10 sec."
sleep 10
cd ~/chives-blockchain/
. ./activate
chives init
#chives init --fix-ssl-permissions
echo "chives version $(chives version)"
deactivate

####pm2 log 0
cd
echo "update completed!"
