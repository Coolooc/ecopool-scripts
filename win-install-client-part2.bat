@echo off

if exist "C:\ecopool-client\" move C:\ecopool-client\config\local.json C:\local.json.bak

rd /s /q C:\ecopool-client\

cd C:\
git clone https://gitlab.com/ecopool-public/chia-telemetery-client.git C:\ecopool-client\

if exist "C:\local.json.bak" move C:\local.json.bak C:\ecopool-client\config\local.json

cd C:\ecopool-client\

cmd /c npm i -g debug

cmd /c npm i -g pm2@4

cmd /c npm i -g pm2-windows-startup

cmd /c pm2 stop all
cmd /c pm2 delete all

cmd /c npm i -g yarn
cmd /c yarn install

cmd /c pm2 start "C:\Program Files\nodejs\node.exe" --name ecopool-gui -- ./backend/app
cmd /c pm2-startup install
cmd /c pm2 save

cmd /c pm2 update
cmd /c pm2 restart all

echo.
echo ===============================================================================
echo Ecopool client available on url http://localhost:3401 open it in your browser
echo ������ ecopool ����㯥� �� ����� http://localhost:3401 ��ன� ��� � ��㧥�
echo ===============================================================================
pause
