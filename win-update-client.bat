@echo off

cmd /c pm2 stop all

for /F "Delims=" %%I in ('dir /b %LOCALAPPDATA%\chia-blockchain\app-*') do set APPFOLDER=%%~I
cd %LOCALAPPDATA%\chia-blockchain\%APPFOLDER%\resources\app.asar.unpacked\daemon\
chia.exe stop all -d
chia.exe stop all
chia.exe stop all -d
chia.exe stop all

for /F "Delims=" %%I in ('dir /b %LOCALAPPDATA%\chives-blockchain\app-*') do set APPFOLDER=%%~I
cd %LOCALAPPDATA%\chives-blockchain\%APPFOLDER%\resources\app.asar.unpacked\daemon\
chives.exe stop all -d
chives.exe stop all
chives.exe stop all -d
chives.exe stop all

cd C:\ecopool-client\
git reset --hard
git pull
cmd /c yarn install

cmd /c pm2 restart all

echo update completed!
pause
